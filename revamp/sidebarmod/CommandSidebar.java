package revamp.sidebarmod;

import net.minecraft.command.CommandException;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.gui.GuiScreen;
import revamp.sidebarmod.gui.screen.GuiScreenSettings;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.CommandBase;

public class CommandSidebar extends CommandBase
{
    private SidebarMod mod;
    
    public CommandSidebar(final SidebarMod mod) {
        this.mod = mod;
    }
    
    public String getName() {
        return "sidebarmod";
    }
    
    public String getUsage(final ICommandSender sender) {
        return "/sidebarmod";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        MinecraftForge.EVENT_BUS.register((Object)this);
    }

    public boolean canCommandSenderUseCommand(final ICommandSender sender) {
        return true;
    }

    public int getRequiredPermissionLevel()
    {
        return 0;
    }
    
    @SubscribeEvent
    public void onClientTick(final TickEvent.ClientTickEvent event) {
        MinecraftForge.EVENT_BUS.unregister((Object)this);
        Minecraft.getMinecraft().displayGuiScreen((GuiScreen)new GuiScreenSettings(this.mod));
    }
}
