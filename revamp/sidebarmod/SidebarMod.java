package revamp.sidebarmod;

import net.minecraft.client.gui.GuiIngame;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraft.command.ICommand;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import revamp.sidebarmod.gui.GuiSidebarIngame;
import revamp.sidebarmod.gui.GuiSidebar;
import java.io.File;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.Mod;

@Mod(modid = "sidebarmod", name = "Sidebar Mod", version = "2.0")
public class SidebarMod
{
    private Minecraft mc;
    private File saveFile;
    private GuiSidebar guiSidebar;
    private GuiSidebarIngame ingame;
    
    public SidebarMod() {
        this.mc = Minecraft.getMinecraft();
    }
    
    @Mod.EventHandler
    public void init(final FMLInitializationEvent event) {
        MinecraftForge.EVENT_BUS.register((Object)this);
        ClientCommandHandler.instance.registerCommand((ICommand)new CommandSidebar(this));
        this.saveFile = new File(this.mc.mcDataDir, "config/SidebarMod.config");
        this.guiSidebar = new GuiSidebar();
        this.ingame = new GuiSidebarIngame(this, this.mc);
        this.loadConfig();
    }
    
    @SubscribeEvent
    public void onClientTick(final TickEvent.ClientTickEvent event) {
        if (!(this.mc.ingameGUI instanceof GuiSidebarIngame)) {
            this.mc.ingameGUI = (GuiIngame)this.ingame;
        }
    }
    
    public GuiSidebar getSidebarGui() {
        return this.guiSidebar;
    }
    
    public void saveConfig() {
        final Configuration config = new Configuration(this.saveFile);
        this.updateConfig(config, false);
        config.save();
    }
    
    public void loadConfig() {
        final Configuration config = new Configuration(this.saveFile);
        config.load();
        this.updateConfig(config, true);
    }
    
    private void updateConfig(final Configuration config, final boolean load) {
        Property prop = config.get("General", "enabled", true);
        if (load) {
            this.guiSidebar.enabled = prop.getBoolean();
        }
        else {
            prop.setValue(this.guiSidebar.enabled);
        }
        prop = config.get("General", "offsetX", 0);
        if (load) {
            this.guiSidebar.offsetX = prop.getInt();
        }
        else {
            prop.setValue(this.guiSidebar.offsetX);
        }
        prop = config.get("General", "offsetY", 0);
        if (load) {
            this.guiSidebar.offsetY = prop.getInt();
        }
        else {
            prop.setValue(this.guiSidebar.offsetY);
        }
        prop = config.get("General", "scale", 1.0);
        if (load) {
            this.guiSidebar.scale = (float)prop.getDouble();
        }
        else {
            prop.setValue((double)this.guiSidebar.scale);
        }
        prop = config.get("General", "rednumbers", true);
        if (load) {
            this.guiSidebar.redNumbers = prop.getBoolean();
        }
        else {
            prop.setValue(this.guiSidebar.redNumbers);
        }
        prop = config.get("General", "shadow", false);
        if (load) {
            this.guiSidebar.shadow = prop.getBoolean();
        }
        else {
            prop.setValue(this.guiSidebar.shadow);
        }
        prop = config.get("Color", "rgb", 0);
        if (load) {
            this.guiSidebar.color = prop.getInt();
        }
        else {
            prop.setValue(this.guiSidebar.color);
        }
        prop = config.get("Color", "alpha", 50);
        if (load) {
            this.guiSidebar.alpha = prop.getInt();
        }
        else {
            prop.setValue(this.guiSidebar.alpha);
        }
        prop = config.get("Chroma", "enabled", false);
        if (load) {
            this.guiSidebar.chromaEnabled = prop.getBoolean();
        }
        else {
            prop.setValue(this.guiSidebar.chromaEnabled);
        }
        prop = config.get("Chroma", "speed", 2);
        if (load) {
            this.guiSidebar.chromaSpeed = prop.getInt();
        }
        else {
            prop.setValue(this.guiSidebar.chromaSpeed);
        }
    }
}
