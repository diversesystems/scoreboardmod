package revamp.sidebarmod.gui.screen;

import org.lwjgl.opengl.GL11;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.client.config.GuiButtonExt;

public class GuiSlider extends GuiButtonExt
{
    public double sliderValue;
    public String dispString;
    public boolean dragging;
    public boolean showDecimal;
    public double minValue;
    public double maxValue;
    public int precision;
    public ISlider parent;
    public String suffix;
    public boolean drawString;
    
    public GuiSlider(final int id, final int xPos, final int yPos, final int width, final int height, final String prefix, final String suf, final double minVal, final double maxVal, final double currentVal, final boolean showDec, final boolean drawStr) {
        this(id, xPos, yPos, width, height, prefix, suf, minVal, maxVal, currentVal, showDec, drawStr, null);
    }
    
    public GuiSlider(final int id, final int xPos, final int yPos, final int width, final int height, final String prefix, final String suf, final double minVal, final double maxVal, final double currentVal, final boolean showDec, final boolean drawStr, final ISlider par) {
        super(id, xPos, yPos, width, height, prefix);
        this.sliderValue = 1.0;
        this.dispString = "";
        this.dragging = false;
        this.showDecimal = true;
        this.minValue = 0.0;
        this.maxValue = 5.0;
        this.precision = 1;
        this.parent = null;
        this.suffix = "";
        this.drawString = true;
        this.minValue = minVal;
        this.maxValue = maxVal;
        this.sliderValue = (currentVal - this.minValue) / (this.maxValue - this.minValue);
        this.dispString = prefix;
        this.parent = par;
        this.suffix = suf;
        this.showDecimal = showDec;
        String val;
        if (this.showDecimal) {
            val = Double.toString(this.sliderValue * (this.maxValue - this.minValue) + this.minValue);
            this.precision = Math.min(val.substring(val.indexOf(".") + 1).length(), 4);
        }
        else {
            val = Integer.toString((int)Math.round(this.sliderValue * (this.maxValue - this.minValue) + this.minValue));
            this.precision = 0;
        }
        this.displayString = this.dispString + val + this.suffix;
        if (!(this.drawString = drawStr)) {
            this.displayString = "";
        }
    }
    
    public GuiSlider(final int id, final int xPos, final int yPos, final String displayStr, final double minVal, final double maxVal, final double currentVal, final ISlider par) {
        this(id, xPos, yPos, 150, 20, displayStr, "", minVal, maxVal, currentVal, true, true, par);
    }
    
    public int getHoverState(final boolean par1) {
        return 0;
    }
    
    protected void mouseDragged(final Minecraft par1Minecraft, final int par2, final int par3) {
        if (this.visible) {
            if (this.dragging) {
                this.sliderValue = (double) (par2 - (this.x + 4)) / (this.width - 8);
                this.updateSlider();
            }
            GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            this.drawTexturedModalRect(this.x + (int)(this.sliderValue * (this.width - 8)), this.y, 0, 66, 4, 20);
            this.drawTexturedModalRect(this.x + (int)(this.sliderValue * (this.width - 8)) + 4, this.y, 196, 66, 4, 20);
        }
    }
    
    public boolean mousePressed(final Minecraft par1Minecraft, final int par2, final int par3) {
        if (super.mousePressed(par1Minecraft, par2, par3)) {
            this.sliderValue = (double) (par2 - (this.x + 4)) / (this.width - 8);
            this.updateSlider();
            return this.dragging = true;
        }
        return false;
    }
    
    public void updateSlider() {
        if (this.sliderValue < 0.0) {
            this.sliderValue = 0.0;
        }
        if (this.sliderValue > 1.0) {
            this.sliderValue = 1.0;
        }
        String val;
        if (this.showDecimal) {
            val = Double.toString(this.sliderValue * (this.maxValue - this.minValue) + this.minValue);
            if (val.substring(val.indexOf(".") + 1).length() > this.precision) {
                val = val.substring(0, val.indexOf(".") + this.precision + 1);
                if (val.endsWith(".")) {
                    val = val.substring(0, val.indexOf(".") + this.precision);
                }
            }
            else {
                while (val.substring(val.indexOf(".") + 1).length() < this.precision) {
                    val += "0";
                }
            }
        }
        else {
            val = Integer.toString((int)Math.round(this.sliderValue * (this.maxValue - this.minValue) + this.minValue));
        }
        if (this.drawString) {
            this.displayString = this.dispString + val + this.suffix;
        }
        if (this.parent != null) {
            this.parent.onChangeSliderValue(this);
        }
    }
    
    public void mouseReleased(final int par1, final int par2) {
        this.dragging = false;
    }
    
    public int getValueInt() {
        return (int)Math.round(this.sliderValue * (this.maxValue - this.minValue) + this.minValue);
    }
    
    public double getValue() {
        return this.sliderValue * (this.maxValue - this.minValue) + this.minValue;
    }
    
    public void setValue(final double d) {
        this.sliderValue = (d - this.minValue) / (this.maxValue - this.minValue);
    }
    
    public interface ISlider
    {
        void onChangeSliderValue(final GuiSlider p0);
    }
}
