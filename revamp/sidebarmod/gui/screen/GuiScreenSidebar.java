package revamp.sidebarmod.gui.screen;

import java.io.IOException;
import net.minecraft.scoreboard.ScoreObjective;
import net.minecraft.client.gui.ScaledResolution;
import revamp.sidebarmod.gui.GuiSidebar;
import revamp.sidebarmod.SidebarMod;
import net.minecraft.client.gui.GuiScreen;

public class GuiScreenSidebar extends GuiScreen
{
    protected SidebarMod mod;
    protected GuiSidebar sidebar;
    private boolean dragging;
    private int lastX;
    private int lastY;
    
    public GuiScreenSidebar(final SidebarMod mod) {
        this.mod = mod;
        this.sidebar = mod.getSidebarGui();
    }
    
    public void drawScreen(final int mouseX, final int mouseY, final float partialTicks) {
        super.drawScreen(mouseX, mouseY, partialTicks);
        if (this.mc.player != null) {
            final ScoreObjective scoreObjective = this.mc.player.getWorldScoreboard().getObjectiveInDisplaySlot(1);
            if (scoreObjective != null) {
                this.sidebar.drawSidebar(scoreObjective, new ScaledResolution(this.mc));
            }
        }
        if (this.dragging) {
            final GuiSidebar sidebar = this.sidebar;
            sidebar.offsetX += mouseX - this.lastX;
            final GuiSidebar sidebar2 = this.sidebar;
            sidebar2.offsetY += mouseY - this.lastY;
        }
        this.lastX = mouseX;
        this.lastY = mouseY;
    }
    
    protected void mouseClicked(final int mouseX, final int mouseY, final int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        if (this.sidebar.contains(mouseX, mouseY)) {
            this.dragging = true;
        }
    }
    
    protected void mouseReleased(final int mouseX, final int mouseY, final int state) {
        super.mouseReleased(mouseX, mouseY, state);
        this.dragging = false;
    }
    
    public void onGuiClosed() {
        this.mod.saveConfig();
    }
}
