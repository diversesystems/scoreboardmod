package revamp.sidebarmod.gui;

import revamp.sidebarmod.gui.screen.GuiScreenSidebar;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.scoreboard.ScoreObjective;
import net.minecraft.client.Minecraft;
import revamp.sidebarmod.SidebarMod;
import net.minecraftforge.client.GuiIngameForge;

public class GuiSidebarIngame extends GuiIngameForge
{
    private SidebarMod mod;
    
    public GuiSidebarIngame(final SidebarMod mod, final Minecraft mc) {
        super(mc);
        this.mod = mod;
    }
    
    protected void renderScoreboard(final ScoreObjective objective, final ScaledResolution res) {
        if (this.mc.currentScreen != null && this.mc.currentScreen instanceof GuiScreenSidebar) {
            return;
        }
        this.mod.getSidebarGui().drawSidebar(objective, res);
    }
}
