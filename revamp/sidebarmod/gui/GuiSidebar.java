package revamp.sidebarmod.gui;

import java.awt.Color;
import java.util.Iterator;
import java.util.List;
import net.minecraft.scoreboard.Scoreboard;
import net.minecraft.scoreboard.Team;
import net.minecraft.util.text.TextFormatting;
import org.lwjgl.opengl.GL11;
import net.minecraft.scoreboard.ScorePlayerTeam;

import net.minecraft.scoreboard.Score;
import java.util.ArrayList;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.scoreboard.ScoreObjective;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;

public class GuiSidebar extends Gui
{
    private FontRenderer fr;
    private int sidebarX;
    private int sidebarY;
    private int sidebarWidth;
    private int sidebarHeight;
    public boolean enabled;
    public boolean redNumbers;
    public boolean shadow;
    public float scale;
    public int offsetX;
    public int offsetY;
    public int color;
    public int alpha;
    public boolean chromaEnabled;
    public int chromaSpeed;
    
    public GuiSidebar() {
        this.fr = Minecraft.getMinecraft().fontRenderer;
    }
    
    public boolean contains(final int mouseX, final int mouseY) {
        final float mscale = this.scale - 1.0f;
        final float minX = this.sidebarX - this.sidebarWidth * mscale;
        final float maxX = minX + this.sidebarWidth * this.scale;
        final float maxY = this.sidebarY + this.sidebarHeight / 2 * mscale;
        final float minY = maxY - this.sidebarHeight * this.scale;
        return mouseX > minX && mouseX < maxX && mouseY > minY - this.fr.FONT_HEIGHT * this.scale && mouseY < maxY;
    }
    
    public void drawSidebar(final ScoreObjective sidebar, final ScaledResolution res) {
        if (!this.enabled) {
            return;
        }
        final FontRenderer fr = Minecraft.getMinecraft().fontRenderer;
        final Scoreboard scoreboard = sidebar.getScoreboard();
        final List<Score> scores = new ArrayList<Score>();
        this.sidebarWidth = fr.getStringWidth(sidebar.getDisplayName());
        for (final Score score : scoreboard.getSortedScores(sidebar)) {
            final String name = score.getPlayerName();
            if (scores.size() < 15 && name != null && !name.startsWith("#")) {
                final Team team = (Team)scoreboard.getPlayersTeam(name);
                final String s2 = this.redNumbers ? (": " + TextFormatting.RED + score.getScorePoints()) : "";
                final String str = ScorePlayerTeam.formatPlayerName(team, name) + s2;
                this.sidebarWidth = Math.max(this.sidebarWidth, fr.getStringWidth(str));
                scores.add(score);
            }
        }
        this.sidebarHeight = scores.size() * fr.FONT_HEIGHT;
        this.sidebarX = res.getScaledWidth() - this.sidebarWidth - 3 + this.offsetX;
        this.sidebarY = res.getScaledHeight() / 2 + this.sidebarHeight / 3 + this.offsetY;
        final int scalePointX = this.sidebarX + this.sidebarWidth;
        final int scalePointY = this.sidebarY - this.sidebarHeight / 2;
        final float mscale = this.scale - 1.0f;
        GL11.glTranslatef((float)(-scalePointX) * mscale, (float)(-scalePointY) * mscale, 0.0f);
        GL11.glScalef(this.scale, this.scale, 1.0f);
        int index = 0;
        for (final Score score2 : scores) {
            ++index;
            final ScorePlayerTeam team2 = scoreboard.getPlayersTeam(score2.getPlayerName());
            final String s3 = ScorePlayerTeam.formatPlayerName((Team)team2, score2.getPlayerName());
            String s4 = TextFormatting.RED + "" + score2.getScorePoints();
            if (!this.redNumbers) {
                s4 = "";
            }
            final int scoreX = this.sidebarX + this.sidebarWidth + 1;
            final int scoreY = this.sidebarY - index * fr.FONT_HEIGHT;
            drawRect(this.sidebarX - 2, scoreY, scoreX, scoreY + fr.FONT_HEIGHT, this.getColor(false));
            this.drawString(s3, this.sidebarX, scoreY, 553648127);
            this.drawString(s4, scoreX - fr.getStringWidth(s4), scoreY, 553648127);
            if (index == scores.size()) {
                final String s5 = sidebar.getDisplayName();
                drawRect(this.sidebarX - 2, scoreY - fr.FONT_HEIGHT - 1, scoreX, scoreY - 1, this.getColor(true));
                drawRect(this.sidebarX - 2, scoreY - 1, scoreX, scoreY, this.getColor(false));
                this.drawString(s5, this.sidebarX + (this.sidebarWidth - fr.getStringWidth(s5)) / 2, scoreY - fr.FONT_HEIGHT, 553648127);
            }
        }
        GL11.glScalef(1.0f / this.scale, 1.0f / this.scale, 1.0f);
        GL11.glTranslatef((float)scalePointX * mscale, (float)scalePointY * mscale, 0.0f);
    }
    
    private int getColor(final boolean darker) {
        int rgb = this.color;
        if (this.chromaEnabled) {
            final long time = 10000L / this.chromaSpeed;
            rgb = Color.HSBtoRGB((float)(System.currentTimeMillis() % time) / (float)time, 0.8f, 0.8f);
        }
        return darker ? this.getColorWithAlpha(rgb, Math.min(255, this.alpha + 10)) : this.getColorWithAlpha(rgb, this.alpha);
    }
    
    private int getColorWithAlpha(final int rgb, final int a) {
        final int r = rgb >> 16 & 0xFF;
        final int g = rgb >> 8 & 0xFF;
        final int b = rgb & 0xFF;
        return a << 24 | r << 16 | g << 8 | b;
    }
    
    private void drawString(final String str, final int x, final int y, final int color) {
        if (this.shadow) {
            this.fr.drawStringWithShadow(str, (float)x, (float)y, color);
        }
        else {
            this.fr.drawString(str, x, y, color);
        }
    }
}
